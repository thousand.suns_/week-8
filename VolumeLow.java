package filter;

public class VolumeLow  Filter{

    @Override
    public List<Games> filter(List<Games> gamesList) {
        List<Games> lowVolume = new ArrayList<>();

        for(Games games : gamesList) {
            if(games.getVolume()<3.0) {
                lowVolume.add(games);
            }
        }
        return lowVolume;
    }
}
