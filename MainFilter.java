package filter;

public class MainFilter {
    public static void main(String[] args) {
        List<Games>gamesList = new ArrayList<>();

        gamesList.add(new Games("CS","Shooter",GO));
        gamesList.add(new Games("The Witcher","Adventure",3));
        gamesList.add(new Games("Zelda","Adventure",4));
        gamesList.add(new Games("Valorant","Shooter",1));
        gamesList.add(new Games("AC Creed","Adventure",5));



        Filter adventureGames = new FilterAdventure();
        Filter shooterGames = new FilterShooter();
        Filter lowVolumeGames = new VolumeLow();
        Filter highVolumeGames = new VolumeHigh();


        Filter adventureLow = new DoubleFilter(adventureGames,lowVolumeGames);
        Filter adventureHigh = new DoubleFilter(adventureGames,highVolumeGames);
        Filter shooterLow = new DoubleFilter(shooterGames,lowVolumeGames);
        Filter shooterHigh = new DoubleFilter(shooterGames,highVolumeGames);


        System.out.println("Adventure games: ");
        printGamesInfo(adventureGames.filter(gamesList));

        System.out.println("Shooter games: ");
        printGamesInfo(shooterGames.filter(gamesList));

        System.out.println("Low volume games: ");
        printGamesInfo(lowVolumeGames.filter(gamesList));

        System.out.println("High Volume games: ");
        printGamesInfo(highVolumeGames.filter(gamesList));

    }
    public static void printGamesInfo(List<Games> gamesList) {
        for(Games games: gamesList) {
            System.out.println("Game info: \nName:" + games.getName() +",Type: "+ games.getType() + ",Volume: "+ games.getVolume()+ "\n");
        }
    }
}
