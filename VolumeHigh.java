package filter;

public class VolumeHigh Filter{

    @Override
    public List<Games> filter(List<Games> gamesList) {
        List<Games> highVolume = new ArrayList<>();

        for(Games games : gamesList) {
            if(games.getVolume()>3.0) {
                highVolume.add(games);
            }
        }
        return highVolume;
    }
}
