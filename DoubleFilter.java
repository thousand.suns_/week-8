package filter;

public class DoubleFilter implements Filter {

    private Filter firstFilter;
    private Filter secondFilter;

    public DoubleFilter(Filter firstFilter,Filter secondFilter) {
        this.firstFilter = firstFilter;
        this.secondFilter = secondFilter;
    }


    @Override
    public List<Games> filter(List<Games> gamesList) {
        List<Games> firstGameFilter = firstFilter.filter(gamesList);
        return secondFilter.filter(firstGameFilter)
    }

}
