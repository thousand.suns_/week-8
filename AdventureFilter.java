package filter;

public class AdventureFilter implements Filter{

    @Override
    public List<Games> filter(List<Games> gamesList) {
        List<Games> adventureGames = new ArrayList<>();

        for(Games games : gamesList) {
            if(games.getType().equalsIgnorcase("Adventure")) {
                adventureGames.add(games);
            }
        }
        return adventureGames;
    }
}
